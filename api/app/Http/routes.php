<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['prefix' => 'auth', 'middleware' => ['JsonApi'], 'namespace' => 'App\Http\Controllers\Auth'], function ($app) {
    $app->post('getToken', 'AuthController@getToken');
});
$app->get('/', function () use ($app) {
    return $app->version();
});

<?php

namespace App\Http\Controllers\Auth;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use \Firebase\JWT\JWT;

$_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../../../../..');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
global $DBType;
$DBType = 'mysql';
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

class AuthController extends BaseController
{
    public function getToken(Request $request)
    {
        $arData = $request->all();
        $user = new \CUser();
        $list = $user->GetList($by, $order, ['LOGIN' => $arData['email']], ['FIELDS' => ['ID','NAME','LAST_NAME', 'PASSWORD']]);
        if ($arUser = $list->GetNext()) {
            if (strlen($arUser["PASSWORD"]) > 32) {
                $salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
                $db_password = substr($arUser["PASSWORD"], -32);
            } else {
                $salt = "";
                $db_password = $arUser["PASSWORD"];
            }
            $user_password = md5($salt . $arData["password"]);
            $passwordCorrect = $db_password === $user_password;
        }
        if ($passwordCorrect === true) {
            $key = env('APP_KEY');
            $token = array(
                "id" => $arUser['ID'],
                "username" => $arUser['NAME'].' '.$arUser['LAST_NAME']
            );
            $jwt = JWT::encode($token, $key);
            return response()->json(['token' => $jwt], 200);
        }
        else
            return response()->json(['status' => 403, 'statusText' => 'error'], 403);
    }
}

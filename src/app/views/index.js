import DocsView from './DocsView';
import HomeView from './HomeView';
import LoginView from './LoginView';
import ProtectedView from './ProtectedView';
import NoMatch from './NoMatch';

export {DocsView};
export {HomeView};
export {LoginView};
export {ProtectedView};
export {NoMatch};

export default {
  DocsView,
  HomeView,
  LoginView,
  ProtectedView,
  NoMatch,
};


import React from 'react';
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import reactMixin from 'react-mixin';
import Card from 'material-ui/lib/card/card';
import CardText from 'material-ui/lib/card/card-text';
import CardTitle from 'material-ui/lib/card/card-title';
import TextField from 'material-ui/lib/text-field';
import RaisedButton from 'material-ui/lib/raised-button';
import * as actionCreators from '../actions';

export class LoginView extends React.Component {
  static propTypes = {
    actions: React.PropTypes.object.isRequired,
    dispatch: React.PropTypes.func,
    isAuthenticating: React.PropTypes.bool,
    location: React.PropTypes.object,
    statusText: React.PropTypes.string,
  };

  constructor(props) {
    super(props);
    const redirectRoute = this.props.location.query.next || '/login';
    this.state = {
      email: '',
      password: '',
      redirectTo: redirectRoute,
    };
  }

  login(e) {
    e.preventDefault();
    this.props.actions.loginUser(this.state.email, this.state.password, this.state.redirectTo);
  }

  render() {
    return (
      <div className="main-container" style={{alignItems:'center'}}>
        <Card>
          <CardTitle>
            <img src="/img/logo.png"/>
          </CardTitle>
          <CardText>
            <TextField valueLink={this.linkState('email')} floatingLabelText="Электронная почта"
              hintText="Электронная почта"
            />
            <br/>
            <TextField errorText={this.props.statusText} valueLink={this.linkState('password')}
              floatingLabelText="Пароль"
              hintText="Пароль"
            />
            <br/>
            <RaisedButton onTouchTap={this.login.bind(this)} label="Войти" secondary={true}/>
          </CardText>
        </Card>
      </div>
    );
  }
}

reactMixin(LoginView.prototype, LinkedStateMixin);

const mapStateToProps = (state) => ({
  isAuthenticating: state.auth.isAuthenticating,
  statusText: state.auth.statusText,
});
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginView);



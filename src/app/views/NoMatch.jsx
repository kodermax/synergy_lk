/** In this file, we create a React component which incorporates components provided by material-ui */
import React from 'react';

export default class NoMatch extends React.Component {
  render() {
    return (
      <div>
        <h1>Page not found</h1>
      </div>
    );
  }
}





import React from 'react';
import Table from 'material-ui/lib/table/table';
import TableBody from 'material-ui/lib/table/table-body';
import TableHeader from 'material-ui/lib/table/table-header';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableRow from 'material-ui/lib/table/table-row';
import TableRowColumn from 'material-ui/lib/table/table-row-column';
import RaisedButton from 'material-ui/lib/raised-button';

export default class DocsView extends React.Component {
  render() {
    return (
      <div style={{margin:10}}>
        <br/>
        <RaisedButton label="Загрузить паспорт"/>
        <br/><br/>
        <RaisedButton label="Загрузить диплом"/>
        <br/>
        <Table>
          <TableHeader>
            <TableRow>
              <TableHeaderColumn colSpan="4" tooltip="История загрузки документов" style={{textAlign: 'center'}}>
                История загрузки документов
              </TableHeaderColumn>
            </TableRow>
            <TableRow>
              <TableHeaderColumn tooltip="Тип документа">Тип документа</TableHeaderColumn>
              <TableHeaderColumn tooltip="Просмотр">Просмотр</TableHeaderColumn>
              <TableHeaderColumn tooltip="Дата загрузки">Дата загрузки</TableHeaderColumn>
              <TableHeaderColumn tooltip="Статус">Статус</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody>
            <TableRow>
              <TableRowColumn>Паспорт</TableRowColumn>
              <TableRowColumn>Скачать</TableRowColumn>
              <TableRowColumn>26.01.2016 14:59</TableRowColumn>
              <TableRowColumn>Ожидает модерации</TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

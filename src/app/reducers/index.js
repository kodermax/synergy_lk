import {combineReducers} from 'redux';
import {routeReducer} from 'react-router-redux';
import auth from './auth';

export default combineReducers({
  auth: auth,
  routing: routeReducer,
});

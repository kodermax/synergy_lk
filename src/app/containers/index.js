import App from './App';
import Root from './Root';

export {App};
export {Root};
export default {
  App,
  Root,
};

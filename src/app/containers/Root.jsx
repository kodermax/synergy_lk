import React from 'react';
import {Provider} from 'react-redux';
import {Route, Router, IndexRoute} from 'react-router';
import {requireAuthentication} from '../components/AuthenticatedComponent';
import App from '../containers/App.jsx';
import {DocsView, HomeView, LoginView, ProtectedView, NoMatch} from '../views';

export default class Root extends React.Component {
  static propTypes = {
    history: React.PropTypes.object.isRequired,
    store: React.PropTypes.object.isRequired,
  };
  render () {
    return (
      <div>
        <Provider store={this.props.store}>
          <div>
            <Router history={this.props.history}>
              <Route path="/" component={App}>
                <Route path="home" component={requireAuthentication(HomeView)}/>
                <Route path="docs" component={requireAuthentication(DocsView)}/>
                <Route path="protected" component={requireAuthentication(ProtectedView)}/>
                <IndexRoute component={requireAuthentication(HomeView)}/>
              </Route>
              <Route path="/login" component={LoginView}/>
              <Route path="*" component={NoMatch} />
            </Router>
          </div>
        </Provider>
      </div>
    );
  }
}


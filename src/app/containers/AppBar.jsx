import React from 'react';
import RaisedButton from 'material-ui/lib/raised-button';
import Toolbar from 'material-ui/lib/toolbar/toolbar';
import ToolbarGroup from 'material-ui/lib/toolbar/toolbar-group';
import Avatar from 'material-ui/lib/avatar';
import MenuItem from 'material-ui/lib/menus/menu-item';
import IconMenu from 'material-ui/lib/menus/icon-menu';
import {routeActions} from 'react-router-redux';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from '../actions';

export class AppBar extends React.Component {
  static propTypes = {
    actions: React.PropTypes.object,
    dispatch: React.PropTypes.func,
  };
  handleNewsView = (e) => {
    e.preventDefault();
    this.props.dispatch(routeActions.push('/'));
  };
  handleDocsView = (e) => {
    e.preventDefault();
    this.props.dispatch(routeActions.push('/docs'));
  };
  handleLogOut = (e) => {
    e.preventDefault();
    this.props.actions.logoutAndRedirect();
  };
  render() {
    return (
      <div>
        <Toolbar>
          <ToolbarGroup firstChild={true} float="left">
            <RaisedButton label="Новости" secondary={true} onTouchTap={this.handleNewsView.bind(this)}/>
            <RaisedButton label="Расписание" secondary={true}/>
            <RaisedButton label="Документы" secondary={true} onTouchTap={this.handleDocsView.bind(this)}/>
            <RaisedButton label="Платежи" secondary={true}/>
          </ToolbarGroup>
          <ToolbarGroup float="right">
            <IconMenu style={{marginTop:5, cursor:'pointer'}}
              iconButtonElement={<Avatar src="http://www.material-ui.com/images/uxceo-128.jpg"/>}
            >
              <MenuItem primaryText="Мой профиль"/>
              <MenuItem primaryText="Настройки"/>
              <MenuItem onTouchTap={this.handleLogOut.bind(this)} primaryText="Выход"/>
            </IconMenu>

          </ToolbarGroup>
        </Toolbar>
      </div>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch),
    dispatch: dispatch,
  };
}
export default connect(state => state, mapDispatchToProps)(AppBar);

import React from 'react';
import {connect} from 'react-redux';
import AppBar from '../containers/AppBar.jsx';
import Card from 'material-ui/lib/card/card';
import CardText from 'material-ui/lib/card/card-text';

@connect((state) => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
  };
})
export default class App extends React.Component {
  static propTypes = {
    children: React.PropTypes.node,
    isAuthenticated: React.PropTypes.bool,
  };
  render () {
    return (
      <div>
        <div className="main-container">
          <div style={{width: 800}}>
            <AppBar/>
            <div style={{backgroundColor: '#FFF', height:'100%'}}>
              <Card>
                <CardText>
                  {this.props.children}
                </CardText>
              </Card>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


import reducer from '../reducers';
import thunkMiddleware from 'redux-thunk';
import {applyMiddleware, compose, createStore} from 'redux';
import {syncHistory} from 'react-router-redux';
import createLogger from 'redux-logger';

export default function configureStore({initialState = {}, history}) {
  const middleware = syncHistory(history);
  const logger = createLogger();
  const finalCreateStore = compose(
    applyMiddleware(middleware, thunkMiddleware, logger)
  )(createStore);
  const store = finalCreateStore(reducer, initialState);
  middleware.listenForReplays(store);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers/index');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;

}

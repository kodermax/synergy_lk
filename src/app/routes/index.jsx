import React from 'react';
import {Route, Router, IndexRoute, browserHistory} from 'react-router';
import {App} from '../containers/App.jsx';
import {HomeView, LoginView, ProtectedView, NoMatch} from '../views';
import {requireAuthentication} from '../components/AuthenticatedComponent';

export default(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={requireAuthentication(HomeView)}/>
      <Route path="login" component={LoginView}/>
      <Route path="protected" component={requireAuthentication(ProtectedView)}/>
    </Route>
    <Route path="*" component={NoMatch} />
  </Router>
);
